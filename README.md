# Nurturejs

_Nurture your environment and let it grow_

Welcome to Nurture JS, an opinionated environment building system for JS

## What is Nurture JS?

Nurture JS sets up your application's environment by building a .env for you, using a file called `environment.json` in your project root based on details you provide in a configuration file.

## What is the rationale for Nurture JS?

If you've ever built a JS application, you'll be aware that your application needs an environment set up before it can run and this can be quite painful. There are plenty of good libraries out there that can do the job to some degree, but NurtureJS has a particular approach that we believe will solve most problems for most people.

We believe:

- Many applications need to handle asynchronous configuration, in which you go to services like AWS to fetch your configuration. Only some frameworks allow async configuration and if you've ever done it, you'll know how hard it can be. With NurtureJS, you load your config before your application starts by generating the `.env` that it needs at that moment and all configuration is synchronous from that point onwards.
- No matter how good an environment generating tool you have or how simple it is to set up an environment for your flashy framework, your build chain will always be limited by the tool that handles the least intelligent config. That is almost always a `.env` file (and for the tiny percentage where it isn't, then this shell one liner will load a .env into your code: `export $(cat .env | sed 's/#.*//g' | xargs)`). nurtureJS aims to support that case and build up from there. Never be surprised by a sudden downgrade in your environment system.
- Documenting your environment is hard. By definition you can't check in your environment to your source control system as you should never check in your secrests, and it's hard to know where any given secret is coming from. With NurtureJS, you document your environment as a natural part of setting it up, meaning that any new people coming into your project will hit the ground running.

## Quick Start

Create an `environment.json` which lays out how to generate your environment.

Example:

```json
{
  "plugins": ["@nurture/aws-plugin"],
  "aws": {
    "dev": {
      "profile": "aws-dev-profile"
    },
    "prod": {
      "profile": "aws-prod-profile"
    }
  },
  "config": {
    "dev": "development",
    "prod": "production"
  },
  "variables": [
    {
      "name": "STATIC_VAR",
      "description": "A variable that you always set tot he same thing",
      "value": "I am set!"
    },
    {
      "name": "CONFIG_VAR",
      "description": "An environment variable you set from a config entry in this file that will be development in the dev environment and production in the prod environment",
      "source": "config"
    },
    {
      "name": "ENVIRONMENT_VAR",
      "description": "An environment variable forwarded on from the environment (and maybe remapped) which we document here",
      "source": "environment",
      "source_input": "FROM_THE_ENVIRONMENT"
    },
    {
      "name": "AWS_VAR",
      "description": "A variable from a 3rd party provider 9in this case the AWS SSM Paramter store, which uses the aws key above to configure where it should fetch its data from, in this case the profiles set",
      "source": "aws",
      "source_input": "/path/to/my/ssm/paramater"
    }
  ]
}
```

Run `yarn nurture -e dev` to generate the `.env` file for the dev environment in the above config file, and `yarn nurture -e prod` to build the prod environment.

## Config File Schema

The config file (`environment.json`) is parsed by nurture to generate your environment .env file. The schema for what it knows about is currently:

### variables:

This key contains a list of all of your variables and where they come from.

The following schema is accepted:
"name" - the environment variable value we will generate an environment variable for
"description" - Used to self-document the environment
"value" - If this is supplied, the environment value is set to this and processing is terminated for this variable.
"source" - Where the variable should be taken from. The source is usually provided by a plugin and Nurture comes with two default plugins, environment and config, for extracting things from the environment and from this config file. Other plugins supply extra values (e.g. the aws-plugin supplies the aws source).
"source_input": Passed directly to the source plugin in order to get the value you want. e.g. the AWS SSM parameter store requires a key path.
"default": If no environment variable is set using any of the above options, this is the value the variable defaults to.

### config:

If you have config variables where you want to set a specific variable name in a specific environment, this is where you put them. e.g.

```json
{
  "config": {
    "dev": {
      "var1": "A value used for development"
    },
    "test": {
      "var1": "The value int he test environment"
    },
    "prod": {
      "var1": "And in production it would be this value"
    }
  }
}
```

Used by the inbuilt config plugin.

### plugins

A list of nurture plugins you wish to initialise. Each plugin may have a config section with a root key it sets (e.g. aws for the aws-plugin).

## Contributing

### Using NX

<a alt="Nx logo" href="https://nx.dev" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="45"></a>

✨ **This workspace has been generated by [Nx, a Smart, fast and extensible build system.](https://nx.dev)** ✨

#### Generate code

If you happen to use Nx plugins, you can leverage code generators that might come with it.

Run `nx list` to get a list of available plugins and whether they have generators. Then run `nx list <plugin-name>` to see what generators are available.

Learn more about [Nx generators on the docs](https://nx.dev/plugin-features/use-code-generators).

#### Running tasks

To execute tasks with Nx use the following syntax:

```
nx <target> <project> <...options>
```

You can also run multiple targets:

```
nx run-many -t <target1> <target2>
```

..or add `-p` to filter specific projects

```
nx run-many -t <target1> <target2> -p <proj1> <proj2>
```

Targets can be defined in the `package.json` or `projects.json`. Learn more [in the docs](https://nx.dev/core-features/run-tasks).

#### Want better Editor Integration?

Have a look at the [Nx Console extensions](https://nx.dev/nx-console). It provides autocomplete support, a UI for exploring and running tasks & generators, and more! Available for VSCode, IntelliJ and comes with a LSP for Vim users.

#### Ready to deploy?

Just run `nx build demoapp` to build the application. The build artifacts will be stored in the `dist/` directory, ready to be deployed.

#### Set up CI!

Nx comes with local caching already built-in (check your `nx.json`). On CI you might want to go a step further.

- [Set up remote caching](https://nx.dev/core-features/share-your-cache)
- [Set up task distribution across multiple machines](https://nx.dev/core-features/distribute-task-execution)
- [Learn more how to setup CI](https://nx.dev/recipes/ci)

#### Connect with us!

- [Join the community](https://nx.dev/community)
- [Subscribe to the Nx Youtube Channel](https://www.youtube.com/@nxdevtools)
- [Follow us on Twitter](https://twitter.com/nxdevtools)
