import { Configuration, NurturePlugin, Value, Variable } from '../../types';

export class EnvironmentPlugin implements NurturePlugin {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor(_configuration: Configuration) {}

  async load(variable: Variable, _environment: string): Promise<Value | undefined> {
    const envName = variable.environment?.key || variable.key || variable.name
    return process.env[envName]
  }

  validateConfig(_environment: string): void {
    // TODO
  }
}
