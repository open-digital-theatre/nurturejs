import { Configuration, NurturePlugin, StaticConfig, Value, Variable } from '../../types';

export class ConfigPlugin implements NurturePlugin {
  protected config: StaticConfig;

  constructor(configuration: Configuration) {
    this.config = configuration.config;
  }

  async load(variable: Variable, environment: string): Promise<Value | undefined> {
    return this.config[environment][variable.name]
  }

  validateConfig(_environment: string): void {
    // TODO
  }
}
