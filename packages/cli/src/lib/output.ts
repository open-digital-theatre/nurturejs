import fs from 'fs';

export function writeEnvironmentFile(environment: Record<string, any>) {
  const envFileKeyValuePairs = Object.entries(environment)
    .map((pair) => {
      return `${pair[0]}="${pair[1]}"`;
    })
    .join('\n');

  // Write the env object to a .env file
  fs.writeFileSync('./.env', envFileKeyValuePairs + '\n');
}