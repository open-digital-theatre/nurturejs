import VariableNotSetError from "./errors/variable-not-set";
import { ConfigPlugin } from "./plugins/config";
import { EnvironmentPlugin } from "./plugins/environment";
import { Configuration, NurturePlugin, Variable } from '../types';

class Application {
  protected plugins: Record<string, NurturePlugin> = {};
  public environment: Record<string, string|number> = {};
  protected sync = false;

  constructor(
    protected readonly configuration: Configuration,
    protected readonly logLevel: number = 1
  ) {
    this.log('Application constructor', 4);
  }

  loadPlugins() {
    this.log('Application loadPlugins', 4);

    const plugins = this.configuration.plugins || [];

    // load default environment plugin
    this.plugins['environment'] = new EnvironmentPlugin(this.configuration);
    this.plugins['config'] = new ConfigPlugin(this.configuration);

    for (const pluginName of plugins) {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const pluginClass = require(pluginName).Plugin;
      const pluginInstance = new pluginClass(this.configuration);

      this.log(pluginInstance, 4);

      this.plugins[pluginInstance.name] = pluginInstance;
    }

    this.log(this.plugins, 4);
  }

  async loadVariable(variable: Variable, environment: string) {
    this.log(`Processing variable ${variable.name}...`, 4);
    if (variable.value) {
      this.log(`Setting variable ${variable.name} to value ${variable.value}`, 4);
      this.environment[variable.name] = variable.value;
    }

    if (Array.isArray(variable.sources)) {
      for (const source of variable.sources) {
        if (this.plugins[source]) {
          this.log(`Looking in plugin ${source} for variable ${variable.name}...`, 4)
          const value = await this.plugins[source].load(variable, environment);
          if (value) {
            this.log(`Setting variable ${variable.name} to ${value} via plugin ${source}`, 3);
            this.environment[variable.name] = value;
            break;
          } else {
            this.log(`Plugin ${source} did not return a value, moving onto next source...`, 4);
          }
        } else {
          this.log(`Plugin ${source} not found, skipping...`, 2);
        }
      }
    } else if (variable.source) {
      if (this.plugins[variable.source]) {
        const value = await this.plugins[variable.source].load(variable, environment);

        if (value) {
          this.log(`Setting variable ${variable.name} to ${value} via plugin ${variable.source}`, 2);
          this.environment[variable.name] = value;
        } else {
          this.log(`Plugin ${variable.source} did not return a value for ${variable.name}`, 2);
        }
      } else {
        this.log(`Plugin ${variable.source} not found, skipping...`, 2);
      }
    }

    // If no value has been given, set a default
    if (!this.environment[variable.name] && variable.default) {
      this.log(`Setting variable ${variable.name} to default value ${variable.default}`, 3);
      this.environment[variable.name] = variable.default;
    }

    // If no value has been set, error out
    if (!this.environment[variable.name]) {
      throw new VariableNotSetError(`Variable ${variable.name} not set`);
    }
  }

  validatePlugins(environment: string) {
    for (const plugin of Object.values(this.plugins)) {
      plugin.validateConfig(environment);
    }
  }

  async run(environment: string) {
    // Now process the variables and load any from external sources
    const variables = this.configuration.variables;

    // Sync output produces nicer user-readable data, but is slower
    if (this.sync) {
      for (const variable of variables) {
        await this.loadVariable(variable, environment);
      }
    } else {
      await Promise.all(variables.map(async (variable) => {
        await this.loadVariable(variable, environment);
      }));
    }
  }

  log(message: NonNullable<unknown>, level = 4) {
    if (level <= this.logLevel) {
      console.log(message.toString());
    }
  }
}

export { Application };
