class VariableNotSetError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'VariableNotSetError';
  }
}

export default VariableNotSetError;
