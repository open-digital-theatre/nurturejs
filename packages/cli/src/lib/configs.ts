import fs from 'fs';
import { Configuration } from '../types';

export async function loadConfiguration(): Promise<Configuration> {
  const environmentFile = fs.readFileSync('./environment.json', 'utf8');
  // Environment file validation??

  return JSON.parse(environmentFile);
}