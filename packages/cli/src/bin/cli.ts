#!/usr/bin/env node

import yargs from 'yargs';
import { Application } from '../lib/app';
import { loadConfiguration } from '../lib/configs';
import { writeEnvironmentFile } from '../lib/output';

export type Arguments = {
  [x: string]: unknown;
  environment: string;
  log: number;
} | Promise<{
  [x: string]: unknown;
  environment: string;
  log: number;
}>;

async function generateEnvironment(options: Arguments) {
  const awaitedOptions = await options;
  const { environment, log } = awaitedOptions;
  if (log > 0) console.log('NurtureJS: Initialising environment variables...');

  const configuration = await loadConfiguration();

  const application = new Application(configuration, log);

  application.loadPlugins();

  try {
    application.validatePlugins(environment);
  } catch(error) {
    console.log("Plugin validation failed.  Please see message below to see what the problem is.");
    console.error(error);
    process.exit(1);
  }

  try {
    await application.run(environment);
  } catch(error) {
    console.error(error);
    process.exit(1);
  }

  writeEnvironmentFile(application.environment);

  if (log > 0) console.log('NurtureJS: Environment variables initialised!');
}

const options: Arguments = yargs(process.argv).option('environment', {
  alias: 'e',
  demandOption: true,
  type: 'string',
  description: 'the environment we are using',
}).option('log', {
  alias: 'l',
  demandOption: false,
  type: 'number',
  description: 'a log level, 0-4, 2 is the default with 0 utterly repressed, 1 gives some basic feedback and 3 and 4 giving more detailed feedback',
  default: 2,
}).version().argv;

generateEnvironment(options);
