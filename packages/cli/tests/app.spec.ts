import { Application } from "../src";

describe("Application test", () => {
  it("should be able to read a static variable", async () => {
    const staticVariableConfig = {
      "variables": [
        {
          "name": "STATIC_VARIABLE",
          "value": "static value"
        }
      ]
    }

    const app = new Application(staticVariableConfig);

    await app.run("dev");

    expect(app.environment).toEqual({
      "STATIC_VARIABLE": "static value"
    })
  });

  it("should be able to read a config variable", async () => {
    const configVariableConfig = {
      "config": {
        "dev": {
          "CONFIG_VARIABLE": "dev value"
        },
        "prod": {
          "CONFIG_VARIABLE": "prod value"
        }
      },
      "variables": [
        {
          "name": "CONFIG_VARIABLE",
          "source": "config",
        }
      ]
    }

    const app = new Application(configVariableConfig);

    app.loadPlugins();

    await app.run("dev");

    expect(app.environment).toEqual({
      "CONFIG_VARIABLE": "dev value"
    });
  });

  it("should be able to read an environment variable", async () => {
    process.env["ENVIRONMENT_VARIABLE"] = "environment value";
    
    const environmentVariableConfig = {
      "variables": [
        {
          "name": "ENVIRONMENT_VARIABLE",
          "source": "environment"
        }
      ]
    }

    const app = new Application(environmentVariableConfig);

    app.loadPlugins();

    await app.run("dev");

    expect(app.environment).toEqual({
      "ENVIRONMENT_VARIABLE": "environment value"
    });
  });

  it("should be able to handle a variable with multiple sources", async () => {
    process.env["MULTIPLE_SOURCE_VARIABLE_1"] = "environment value 1";
    process.env["MULTIPLE_SOURCE_VARIABLE_2"] = "environment value 2";
    
    const multipleSourceVariableConfig = {
      "config": {
        "dev": {
          "MULTIPLE_SOURCE_VARIABLE_1": "dev value",
          "MULTIPLE_SOURCE_VARIABLE_2": "dev value",
        },
        "prod": {
          "MULTIPLE_SOURCE_VARIABLE_1": "prod value",
          "MULTIPLE_SOURCE_VARIABLE_2": "prod value",
        }
      },
      "variables": [
        {
          "name": "MULTIPLE_SOURCE_VARIABLE_1",
          "sources": [
            "config",
            "environment",
          ]
        },
        {
          "name": "MULTIPLE_SOURCE_VARIABLE_2",
          "sources": [
            "environment",
            "config",
          ]
        }
      ]
    };

    const app = new Application(multipleSourceVariableConfig);

    app.loadPlugins();

    await app.run("dev");

    expect(app.environment).toEqual({
      "MULTIPLE_SOURCE_VARIABLE_1": "dev value",
      "MULTIPLE_SOURCE_VARIABLE_2": "environment value 2",
    });
  });
});
