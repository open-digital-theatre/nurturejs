import { SSMClient, GetParameterCommand } from '@aws-sdk/client-ssm';
import { fromIni } from '@aws-sdk/credential-providers';
import { AwsConfig, Configuration, NurturePlugin, Variable, Value } from '../types';

export class AwsPlugin implements NurturePlugin {
  protected configuration: AwsConfig;
  public name = 'aws';

  constructor(configuration: Configuration) {
    this.configuration = configuration['aws'];
  }

  validateConfig(environment: string) {
    if (!(environment in this.configuration)) {
      throw new Error(`No AWS profile or region set for environment ${environment}.  Please add an environment key containing your AWS settings for the environment.`);
    }
  }

  async load(variable: Variable, environment: string): Promise<Value | undefined> {
    const awsParams = this.configuration[environment];

    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const ssmClient = new SSMClient({
      credentials: fromIni({profile: awsParams.profile as string}),
      region: awsParams.region as string,
    });

    const parameter = variable.aws?.parameter || variable.parameter;
    const command = new GetParameterCommand({
      Name: parameter,
      WithDecryption: true,
    });
    try {
      const response = await ssmClient.send(command);

      return response.Parameter?.Value;
    } catch(e) {
      return undefined;
    }
  }
}
