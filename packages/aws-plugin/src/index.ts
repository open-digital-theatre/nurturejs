import { AwsPlugin } from "./lib/aws-plugin";

export {
  AwsPlugin,
  AwsPlugin as Plugin,
}
