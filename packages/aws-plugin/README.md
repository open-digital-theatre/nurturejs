# AWS Nurture plugin

This plugin is a very simple way of accessing AWS SSM parameters from Nurture.

## Usage

When loading, add a top level `aws` key indexed by environment to map your environments to profiles and regions. The `profile` and `region` key are picked up and intepreted in order ot fetch the values from the correct profile.

This assumes that you have an `.aws.config` and `.aws/credentials` set up in your home directory.

Example usage:

```json
{
  "aws": {
    "dev": {
      "region": "us-east-1",
      "profile": "aws-dev-profile"
    },
    "prod": {
      "region": "us-east-1",
      "profile": "aws-prod-profile"
    }
  },
  "variables": [
    {
      "name": "AWS_VAR",
      "description": "An AWS SSM parameter variable",
      "source": "aws",
      "parameter": "/path/to/ssm/key"
    }
  ]
}
```

If using with mutliple sources, you can use the following syntax to prevent name collision:

```
  "variables": [
    {
      "name": "AWS_VAR",
      "description": "An AWS SSM parameter variable",
      "sources": ["environment", "aws"],
      "aws": {
        "parameter": "/path/to/ssm/key"
      }
    }
  ]
}
```
