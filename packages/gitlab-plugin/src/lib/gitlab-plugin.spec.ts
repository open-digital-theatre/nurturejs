import { gitlabPlugin } from './gitlab-plugin';

describe('gitlabPlugin', () => {
  it('should work', () => {
    expect(gitlabPlugin()).toEqual('gitlab-plugin');
  });
});
