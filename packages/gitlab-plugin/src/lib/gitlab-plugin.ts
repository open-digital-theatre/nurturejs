import fetch from "isomorphic-fetch";
import * as fs from "fs";
import path from "path";
import { Configuration, GitlabConfig, NurturePlugin, Value, Variable } from '../types';

function resolveTilde(path: string) {
  if (path[0] === '~') {
    return path.replace('~', process.env["HOME"] || '');
  }
  return path;
}

export class GitlabPlugin implements NurturePlugin {
  protected configuration: GitlabConfig;
  public name = 'gitlab';

  constructor(configuration: Configuration) {
    this.configuration = configuration['gitlab'];
  }

  validateConfig(environment: string) {
    if (environment in this.configuration) {
      throw Error(`No gitlab parameters for environment ${environment}.  Please add an environment key containing your project ID for each environment.`);
    }
  }

  async load(variable: Variable, environment: string): Promise<Value | undefined> {
    const gitlabParams = this.configuration[environment];

    const credentialsPath = gitlabParams.credentialsPath || "~/.gitlab/credentials";
    const processedPath = resolveTilde(credentialsPath);
    const absolutePath = path.resolve(processedPath);

    const credentials = fs.readFileSync(absolutePath, 'utf8').trim();
    
    const projectId = gitlabParams.projectId;
    const key = variable.gitlab?.key || variable.key || variable.name;

    const response = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/variables/${key}`, {
      headers: {
        'PRIVATE-TOKEN': credentials,
      }
    });

    if (!response.ok) {
      throw new Error(`Failed to fetch variable ${key} from Gitlab`);
    }

    const gitlabVariable = await response.json();

    return gitlabVariable.value;
  }
}
