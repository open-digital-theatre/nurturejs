import { GitlabPlugin } from "./lib/gitlab-plugin";

export {
  GitlabPlugin,
  GitlabPlugin as Plugin,
}
