export interface NurturePlugin {
  load(variable: Variable, environment: string): Promise<Value | undefined>;
  validateConfig(environment: string): void;
}

export type Configuration = {
  plugins: string[];
  aws: AwsConfig;
  gitlab: GitlabConfig;
  config: StaticConfig;
  variables: Variable[];
};

export type AwsConfig = {
  [env: string]: {
    profile: string;
    region: string;
  }
};

type GitlabConfig = {
  [env: string]: {
    credentialsPath: string;
    projectId: number;
  }
}

export type StaticConfig = {
  [env: string]: ConfigVariables;
};

type ConfigVariables = {
  [varName: string]: string | number;
}

type Variable = {
  name: string;
  description: string;
  environment?: {
    key: string;
  };
  gitlab?: {
    key: string;
  }
  key?: string;
  source?: string;
  sources?: string[];
  aws?: {
    parameter: string;
  };
  parameter?: string;
  value?: Value;
  default?: string | number;
}

type Value = string | number;
