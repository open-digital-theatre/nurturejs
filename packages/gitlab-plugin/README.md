# Gitlab Nurture plugin

Access Gitlab project parameters from Nurture in this plugin.

## Usage

Add a top level "gitlab" key to your config. At the moment, we take two parameters:

- credentialsPath: A path to a file containing a gitlab key that can be used to access the repo
- projectId: The project ID (available in the first page of the settings)

`credentialsPath` defaults to `~/.gitlab/credentials`

Example usage:

```json
{
  "aws": {
    "dev": {
      "projectId": 1234,
      "credentialsPath": "~/.gitlab/credentials"
    },
    "prod": {
      "projectId": 1234,
      "credentialsPath": "~/.gitlab/credentials"
    }
  },
  "variables": [
    {
      "name": "GITLAB_VAR",
      "description": "A project gitlab variable (you don't need key if you are not renaming the variable)",
      "source": "gitlab",
      "key": "I_NAMED_IT_SOMETHING_ELSE_IN_GITLAB"
    }
  ]
}
```

If using with mutliple sources, you can use the following syntax to prevent name collision:

```
  "variables": [
    {
      "name": "GITLAB_VAR",
      "description": "A project gitlab variable (you don't need key if you are not renaming the variable)",
      "source": "gitlab",
      "gitlab": {
        "key": "I_NAMED_IT_SOMETHING_ELSE_IN_GITLAB"
      }
    }
  ]
}
```
